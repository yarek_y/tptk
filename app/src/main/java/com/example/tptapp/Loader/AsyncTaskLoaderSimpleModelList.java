package com.example.tptapp.Loader;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.example.tptapp.data.SimpleModel;

import java.util.Collections;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class AsyncTaskLoaderSimpleModelList extends AsyncTaskLoader<List<SimpleModel>> {
    final private static String URL = "http://dev.tapptic.com/test/json.php";
    public static final int LOADER_ID = 1;
    private OkHttpClient mClient = new OkHttpClient();
    private List<SimpleModel> mSimpleModels;

    public AsyncTaskLoaderSimpleModelList(@NonNull Context context) {

        super(context);
    }


    @Nullable
    @Override
    public List<SimpleModel> loadInBackground() {

        if(mSimpleModels!= null && mSimpleModels.size()>0){
            return mSimpleModels;
        }

        Request request = new Request.Builder().url(URL)
                                               .build();

        try {
            Response response = mClient.newCall(request)
                                       .execute();

            final String responseStr = response.body()
                                               .string();

            mSimpleModels = SimpleModel.fromJsonString(responseStr);
            return mSimpleModels;

        }
        catch(Exception pE) {
            Log.e("AsyncTaskLoader", "loadInBackground: ", pE);
        }

        return Collections.EMPTY_LIST;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }
}
