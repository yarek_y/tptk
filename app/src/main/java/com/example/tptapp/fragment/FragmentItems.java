package com.example.tptapp.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.tptapp.Loader.AsyncTaskLoaderSimpleModelList;
import com.example.tptapp.R;
import com.example.tptapp.data.SimpleModel;
import com.example.tptapp.logic.IAppLogic;

import java.util.Collections;
import java.util.List;


public class FragmentItems extends Fragment implements LoaderManager.LoaderCallbacks<List<SimpleModel>> {

    private IAppLogic mAppLogic;
    private RecyclerViewAdapterItems mAdapter;
    private RecyclerView mRecyclerView;

    public FragmentItems() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        mAdapter = new RecyclerViewAdapterItems(Collections.EMPTY_LIST, mAppLogic);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mRecyclerView = (RecyclerView) inflater.inflate(R.layout.fragment_items_list, container, false);

        mRecyclerView.setAdapter(mAdapter);

        getActivity().getSupportLoaderManager()
                     .initLoader(AsyncTaskLoaderSimpleModelList.LOADER_ID, null, this);
        if(mAppLogic != null) {
            mAppLogic.onLoadingStart();
        }

        return mRecyclerView;
    }

    public void reload() {

        getActivity().getSupportLoaderManager()
                     .restartLoader(AsyncTaskLoaderSimpleModelList.LOADER_ID, null, this);
    }

    @Override
    public void onAttach(Context context) {

        super.onAttach(context);
        if(context instanceof IAppLogic) {
            mAppLogic = (IAppLogic) context;
        }
        else {
            throw new RuntimeException(context.toString() + " must implement IAppLogic");
        }
    }

    @Override
    public void onDetach() {

        super.onDetach();
        mAppLogic = null;
    }

    @NonNull
    @Override
    public Loader<List<SimpleModel>> onCreateLoader(int id, @Nullable Bundle args) {

        return new AsyncTaskLoaderSimpleModelList(getContext());
    }

    @Override
    public void onLoadFinished(@NonNull Loader<List<SimpleModel>> loader, List<SimpleModel> pData) {

        if(mAppLogic != null) {
            mAppLogic.onLoadingFinished();
        }

        if(pData != null && pData.size() > 0) {
            mAppLogic.onItemsLoaded(pData);
            mAdapter.setValues(pData);
            mRecyclerView.scrollToPosition(mAppLogic.getSelectedItem());
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<List<SimpleModel>> loader) {

    }


}
