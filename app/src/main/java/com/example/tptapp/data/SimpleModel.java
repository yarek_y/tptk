package com.example.tptapp.data;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A dummy item representing a piece of name.
 */
public class SimpleModel {
    public final String image;
    public final String name;

    public SimpleModel(String image, String name) {

        this.image = image;

        this.name = name;
    }

    public SimpleModel(JSONObject pJsonObject) throws JSONException {
        image = pJsonObject.getString("image");
        name = pJsonObject.getString("name");

    }

    public static List<SimpleModel> fromJsonString(String pResponseStr) throws JSONException {

        ArrayList<SimpleModel> ret = new ArrayList<>();
        final JSONArray jsonArray = new JSONArray(pResponseStr);

        for(int i = 0; i < jsonArray.length(); i++) {
            final JSONObject jsonObject = jsonArray.getJSONObject(i);
            ret.add(new SimpleModel(jsonObject));
        }

        return ret;
    }

    @Override
    public String toString() {

        return "SimpleModel{" + "image='" + image + '\'' + ", name='" + name + '\'' + '}';
    }
}
