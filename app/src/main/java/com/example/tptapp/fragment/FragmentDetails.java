package com.example.tptapp.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tptapp.Loader.AsyncTaskLoaderSimpleModelDetails;
import com.example.tptapp.R;
import com.example.tptapp.data.SimpleModelDetailed;
import com.example.tptapp.logic.IAppLogic;
import com.squareup.picasso.Picasso;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentDetails extends Fragment implements LoaderManager.LoaderCallbacks<SimpleModelDetailed> {
    public static final String KEY_NAME = "KEY_NAME";

    private TextView mTextViewText;
    private ImageView mImageViewImage;
    private IAppLogic mAppLogic;

    public FragmentDetails() {
        // Required empty public constructor
    }

    public static FragmentDetails newInstance(String pName){

        final FragmentDetails ret = new FragmentDetails();

        Bundle bundle = new Bundle();
        bundle.putString(KEY_NAME, pName);
        ret.setArguments(bundle);

        return ret;
    }

    public static FragmentDetails newInstance(Bundle pArgs){

        final FragmentDetails ret = new FragmentDetails();
        ret.setArguments(pArgs);
        return ret;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_details, container, false);

        mTextViewText = view.findViewById(R.id.text);
        mImageViewImage = view.findViewById(R.id.image);

        getActivity().getSupportLoaderManager()
                     .restartLoader(AsyncTaskLoaderSimpleModelDetails.LOADER_ID, getArguments(), this);
        if(mAppLogic != null) {
            mAppLogic.onLoadingStart();
        }

        return view;
    }

    public void reload() {

        getActivity().getSupportLoaderManager()
                     .restartLoader(AsyncTaskLoaderSimpleModelDetails.LOADER_ID, getArguments(), this);
    }

    @Override
    public void onAttach(Context context) {

        super.onAttach(context);
        if(context instanceof IAppLogic) {
            mAppLogic = (IAppLogic) context;
        }
        else {
            throw new RuntimeException(context.toString() + " must implement IAppLogic");
        }
    }

    @Override
    public void onDetach() {

        super.onDetach();
        mAppLogic = null;
    }


    @NonNull
    @Override
    public Loader<SimpleModelDetailed> onCreateLoader(int id, @Nullable Bundle args) {


        String param = null;

        if(args != null) {
            param = args.getString(KEY_NAME);
        }
        return new AsyncTaskLoaderSimpleModelDetails(getContext(), param);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<SimpleModelDetailed> loader, SimpleModelDetailed pData) {

        if(pData != null) {
            Picasso.get()
                   .load(pData.image)
                   .into(mImageViewImage);
            mTextViewText.setText(pData.text);
        }

        if(mAppLogic != null) {
            mAppLogic.onLoadingFinished();
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<SimpleModelDetailed> loader) {

    }
}
