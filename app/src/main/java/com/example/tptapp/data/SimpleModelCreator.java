package com.example.tptapp.data;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Helper class for providing sample name for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class SimpleModelCreator {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<SimpleModel> ITEMS = new ArrayList<>();


    private static final int COUNT = 25;

    static {
        // Add some sample items.
        for(int i = 1; i <= COUNT; i++) {
            addItem(createDummyItem(i));
        }
    }

    private static void addItem(SimpleModel item) {

        ITEMS.add(item);
    }

    private static SimpleModel createDummyItem(int position) {

        return new SimpleModel("https://picsum.photos/200/200?id="+position, "Item " + position);
    }


}
