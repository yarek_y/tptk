package com.example.tptapp.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.tptapp.R;
import com.example.tptapp.data.SimpleModel;
import com.example.tptapp.fragment.FragmentDetails;
import com.example.tptapp.fragment.FragmentItems;
import com.example.tptapp.logic.IAppLogic;

import java.util.List;

import static android.content.res.Configuration.ORIENTATION_LANDSCAPE;

public class ActivityMain extends AppCompatActivity implements IAppLogic, SwipeRefreshLayout.OnRefreshListener {

    public static final String EXTRA_SELECTED = "EXTRA_SELECTED";
    public static final String EXTRA_SELECTED_INDEX = "EXTRA_SELECTED_INDEX";
    public static final String BACKSTACK_NAME = "detailed";
    public static final String SHARED_PREFS_NAME = "TPT_APP";
    private View mViewNetworkBanner;
    private SwipeRefreshLayout mRefreshLayout;
    private FragmentItems mFragmentItems;

    private BroadcastReceiver mNetworkChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            checkConnectionState(context);
        }
    };
    private String mSelectedName = null;
    private int mSelectedIndex = -1;

    private void checkConnectionState(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        handleConnectionChange(isConnected);
    }

    private void handleConnectionChange(boolean pIsConnected) {

        mViewNetworkBanner.setVisibility(pIsConnected ? View.GONE : View.VISIBLE);

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);
        outState.putString(EXTRA_SELECTED, mSelectedName);
        outState.putInt(EXTRA_SELECTED_INDEX, mSelectedIndex);

        saveData();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {

        super.onRestoreInstanceState(savedInstanceState);
        restoreData(savedInstanceState);
    }

    private void restoreData(Bundle savedInstanceState) {

        mSelectedName = savedInstanceState.getString(EXTRA_SELECTED, null);
        mSelectedIndex = savedInstanceState.getInt(EXTRA_SELECTED_INDEX, -1);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mViewNetworkBanner = findViewById(R.id.network_banner);
        mRefreshLayout = findViewById(R.id.refresh);
        mFragmentItems = (FragmentItems) getSupportFragmentManager().findFragmentById(R.id.fragment_items);

        IntentFilter intentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(mNetworkChangeReceiver, intentFilter);

        mRefreshLayout.setOnRefreshListener(this);


        if(savedInstanceState != null) {
            restoreData(savedInstanceState);
        }
        else {
            restoreData();
            if(mSelectedName != null) {
                getSupportFragmentManager().beginTransaction()
                                           .replace(R.id.container, FragmentDetails.newInstance(mSelectedName))
                                           .addToBackStack(BACKSTACK_NAME)
                                           .commit();
            }
        }

    }

    private void saveData() {

        final SharedPreferences sp = getSharedPreferences(SHARED_PREFS_NAME, MODE_PRIVATE);
        sp.edit()
          .putInt(EXTRA_SELECTED_INDEX, mSelectedIndex)
          .putString(EXTRA_SELECTED, mSelectedName)
          .commit();

    }

    private void restoreData() {

        final SharedPreferences sp = getSharedPreferences(SHARED_PREFS_NAME, MODE_PRIVATE);
        mSelectedIndex = sp.getInt(EXTRA_SELECTED_INDEX, -1);
        mSelectedName = sp.getString(EXTRA_SELECTED, null);

    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
        unregisterReceiver(mNetworkChangeReceiver);
    }

    @Override
    public void onListFragmentInteraction(int pPosition, SimpleModel item) {

        mSelectedIndex = pPosition;
        mSelectedName = item.name;

        getSupportFragmentManager().beginTransaction()
                                   .replace(R.id.container, FragmentDetails.newInstance(item.name))
                                   .addToBackStack(BACKSTACK_NAME)
                                   .commit();

    }

    @Override
    public void onLoadingStart() {

        if(mRefreshLayout != null) {
            mRefreshLayout.setRefreshing(true);
        }
    }

    @Override
    public void onLoadingFinished() {

        if(mRefreshLayout != null) {
            mRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onItemsLoaded(List<SimpleModel> pData) {

        final boolean isTabletMode = getResources().getBoolean(R.bool.tablet_mode);
        final boolean isHorizontal = getResources().getConfiguration().orientation == ORIENTATION_LANDSCAPE;

        if(isTabletMode && pData != null && pData.size() > 0 && mSelectedIndex == -1 && isHorizontal) {
            onListFragmentInteraction(0, pData.get(0));
        }
    }

    @Override
    public int getSelectedItem() {

        return mSelectedIndex;
    }

    @Override
    public void onBackPressed() {

        if(getSupportFragmentManager().getBackStackEntryCount() == 0) {
            super.onBackPressed();
        }
        else {
            getSupportFragmentManager().popBackStack(BACKSTACK_NAME, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            mSelectedName = null;
        }
    }

    @Override
    public void onRefresh() {

        mRefreshLayout.setRefreshing(true);
        mFragmentItems.reload();

        if(mSelectedName != null) {
            getSupportFragmentManager().beginTransaction()
                                       .replace(R.id.container, FragmentDetails.newInstance(mSelectedName))
                                       .addToBackStack(BACKSTACK_NAME)
                                       .commit();
        }
    }
}
