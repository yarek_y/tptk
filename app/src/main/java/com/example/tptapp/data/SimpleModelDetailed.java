package com.example.tptapp.data;

import org.json.JSONException;
import org.json.JSONObject;

public class SimpleModelDetailed extends SimpleModel {
    public final String text;

    public SimpleModelDetailed(String image, String name, String pText) {

        super(image, name);
        text = pText;
    }

    public SimpleModelDetailed(JSONObject pJsonObject) throws JSONException {

        super(pJsonObject);
        text = pJsonObject.getString("text");
    }


    @Override
    public String toString() {

        return "SimpleModelDetailed{" + "text='" + text + '\'' + ", image='" + image + '\'' + ", name='" + name + '\'' + '}';
    }
}
