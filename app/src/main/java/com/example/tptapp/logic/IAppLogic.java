package com.example.tptapp.logic;

import com.example.tptapp.data.SimpleModel;

import java.util.List;

public interface IAppLogic {
    void onListFragmentInteraction(int pPosition, SimpleModel item);
    void onLoadingStart();
    void onLoadingFinished();
    void onItemsLoaded(List<SimpleModel> pData);
    int getSelectedItem();
}
