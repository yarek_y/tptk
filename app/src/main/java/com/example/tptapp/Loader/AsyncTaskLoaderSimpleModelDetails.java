package com.example.tptapp.Loader;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.example.tptapp.data.SimpleModelDetailed;

import org.json.JSONObject;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class AsyncTaskLoaderSimpleModelDetails extends AsyncTaskLoader<SimpleModelDetailed> {
    final private static String URL = "http://dev.tapptic.com/test/json.php";
    public static final int LOADER_ID = 2;
    private OkHttpClient mClient = new OkHttpClient();
    private String mParameter;
    private SimpleModelDetailed mDetailed;


    public AsyncTaskLoaderSimpleModelDetails(@NonNull Context context, String pParameter) {

        super(context);
        mParameter = pParameter;
    }

    public void setParameter(String pParameter){
        mParameter = pParameter;
    }

    public String getParameter() {

        return mParameter;
    }

    @Nullable
    @Override
    public SimpleModelDetailed loadInBackground() {

        if(mDetailed != null) {
            return mDetailed;
        }

        if(mParameter == null || mParameter.isEmpty()){
            return null;
        }

        HttpUrl url = HttpUrl.parse(URL).newBuilder().addQueryParameter("name", mParameter).build();

        Request request = new Request.Builder().url(url)
                                               .build();

        try {
            Response response = mClient.newCall(request)
                                       .execute();

            final String responseStr = response.body()
                                               .string();

            mDetailed = new SimpleModelDetailed(new JSONObject(responseStr));
            return mDetailed;

        }
        catch(Exception pE) {
            Log.e("AsyncTaskLoader", "loadInBackground: ", pE);
        }

        return null;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }
}
