package com.example.tptapp.fragment;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tptapp.R;
import com.example.tptapp.data.SimpleModel;
import com.example.tptapp.logic.IAppLogic;
import com.squareup.picasso.Picasso;

import java.util.List;


public class RecyclerViewAdapterItems extends RecyclerView.Adapter<RecyclerViewAdapterItems.ViewHolder> {

    private final IAppLogic mListener;
    private List<SimpleModel> mValues;
    private int mFocusedItem = -1;

    public RecyclerViewAdapterItems(List<SimpleModel> items, IAppLogic pAppLogic) {

        mValues = items;
        mListener = pAppLogic;
        mFocusedItem = pAppLogic.getSelectedItem();
    }

    public void setValues(List<SimpleModel> pValues) {

        mFocusedItem = mListener.getSelectedItem();
        mValues = pValues;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                                  .inflate(R.layout.cell_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.mView.setSelected(position == mFocusedItem);
        holder.mItem = mValues.get(position);
        holder.mContentView.setText(mValues.get(position).name);
        Picasso.get()
               .load(mValues.get(position).image)
               .into(holder.mImageView);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                notifyItemChanged(mFocusedItem);
                mFocusedItem = position;
                notifyItemChanged(mFocusedItem);

                if(null != mListener) {
                    mListener.onListFragmentInteraction(position, holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {

        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mContentView;
        public final ImageView mImageView;
        public SimpleModel mItem;

        public ViewHolder(View view) {

            super(view);
            mView = view;
            mContentView = view.findViewById(R.id.name);
            mImageView = view.findViewById(R.id.image);
        }

        @Override
        public String toString() {

            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
